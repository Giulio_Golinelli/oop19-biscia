package it.unibo.biscia.desktop;

import it.unibo.biscia.Biscia;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

/**
 * entry point of desktop application.
 *
 */
public final class DesktopLauncher {

    private DesktopLauncher() {
    }

    /**
     * entry point of desktop application.
     * 
     * @param arg arguments from command line
     */
    public static void main(final String[] arg) {
        final Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle(Biscia.Constants.WINDOW_TITLE);
        config.setWindowedMode(Biscia.Constants.WINDOW_WIDTH, Biscia.Constants.WINDOW_HEIGHT);
        config.setResizable(false);
        new Lwjgl3Application(new Biscia(), config);
    }
}

package it.unibo.biscia.view.managers;

/**
 * Utility class for managing Musics files.
 *
 */
public final class MusicManager {

    /**
     * Biscia's main theme.
     */
    public static final Asset<Music> MAIN_THEME = new AssetImpl<>("musics/Main_Theme.mp3", "main_theme", new Music());

    /**
     * A sound asset info.
     *
     */
    public static final class Music {
        private Music() {
        }
    }

    private MusicManager() {
    }

}

package it.unibo.biscia.core;

import it.unibo.biscia.core.EntityManaged.Movable;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * read various information from level.
 *
 */
final class Levels {

    private Levels() {
    }

    public static List<EntityManaged> getIntersecatedEntity(final LevelManaged level) {
        return Levels.entitiesOnCells(level, Levels.getIntersectionsCells(level)).stream().collect(Collectors.toList());
    }

    public static List<SmartCell> getIntersectionsCells(final LevelManaged level) {
        // tutte le celle con più di una entità dentro più tutte le celle presenti più
        // volte in una entità
        final List<SmartCell> cells = new LinkedList<>(Levels.getOccupiedCells(level).stream()
                .filter(cell -> Levels.entitiesOnCell(level, cell).size() > 1).collect(Collectors.toList()));
        for (final var e : level.getEntitiesManaged()) {
            if (e.getSmartCells().size() != e.getSmartCells().stream().distinct().count()) {
                for (final SmartCell c : e.getSmartCells()) {
                    if (e.getSmartCells().indexOf(c) != e.getSmartCells().lastIndexOf(c)) {
                        cells.add(c);
                    }
                }
            }
        }
        return cells;
    }

    public static List<EntityManaged> entitiesOnCells(final LevelManaged level, final List<SmartCell> cells) {
        return Collections.unmodifiableList(cells.stream().flatMap(c -> Levels.entitiesOnCell(level, c).stream())
                .distinct().collect(Collectors.toList()));
    }

    public static List<EntityManaged> entitiesOnCell(final LevelManaged level, final SmartCell cell) {
        return Collections.unmodifiableList(level.getEntities().stream().filter(e -> e instanceof EntityManaged)
                .map(e -> (EntityManaged) e).filter(e -> e.getCells().contains(cell)).collect(Collectors.toList()));
    }

    public static List<SmartCell> getOccupiedCells(final LevelManaged level) {
        return Collections.unmodifiableList(level.getEntitiesManaged().stream().flatMap(e -> e.getSmartCells().stream())
                .distinct().collect(Collectors.toList()));
    }

    public static List<SmartCell> getFreeCells(final LevelManaged level) {
        final var occ = Levels.getOccupiedCells(level);
        return Collections.unmodifiableList(
                level.getCells().stream().filter(c -> !(occ.contains(c))).collect(Collectors.toList()));
    }

    public static List<EntityManaged.Eatable> getEatables(final LevelManaged level) {
        return level.getEntities().stream().filter(e -> e instanceof EntityManaged.Eatable)
                .map(e -> (EntityManaged.Eatable) e).collect(Collectors.toList());
    }

    public static List<EntityManaged.Eater> getEater(final LevelManaged level) {
        return level.getEntities().stream().filter(e -> e instanceof EntityManaged.Eater)
                .map(e -> (EntityManaged.Eater) e).collect(Collectors.toList());
    }

    public static List<Entity> getEntityOfType(final LevelManaged level, final EntityType entityType) {

        return level.getEntities().stream().filter(e -> e.getType().equals(entityType)).collect(Collectors.toList());

    }

    private static boolean isFreeNearest(final LevelManaged level, final SmartCell cell) {
        final Set<SmartCell> occ = Levels.getOccupiedCells(level).stream().collect(Collectors.toSet());
        for (int c = -1; c < 2; c++) {
            for (int r = -1; r < 2; r++) {
                if (occ.contains(level.getCell(cell.getCol() + c, cell.getRow() + r))) {
                    return false;
                }

            }
        }
        return true;
    }

    public static SmartCell getNewSnakePosition(final LevelManaged level) {
        final SmartCell cell;
        if (Levels.getDirectables(level).isEmpty()) {
            cell = level.getCell(level.getCols() / 3 * 2, level.getRows() / 2);
        } else {
            cell = level.getCell(level.getCols() / 3, level.getRows() / 2);
        }
        if (!Levels.getOccupiedCells(level).contains(cell) && Levels.isFreeNearest(level, cell)) {
            return cell;
        }
        final List<SmartCell> cells = Levels.getFreeCells(level);
        List<SmartCell> cellsFree;
        cellsFree = cells.stream().filter(c -> Levels.isFreeNearest(level, c)).collect(Collectors.toList());
        if (cellsFree.isEmpty()) {
            cellsFree = cells.stream().filter(c -> cells.contains(level.getSideCell(c, Direction.LEFT)))
                    .filter(c -> cells.contains(level.getSideCell(c, Direction.RIGHT))).collect(Collectors.toList());
        }
        if (cellsFree.isEmpty()) {
            cellsFree = cells;
        }
        return cells.get(new Random().nextInt(cells.size()));
    }

    public static List<Movable> getMovables(final LevelManaged level) {
        return level.getEntities().stream().filter(e -> e instanceof EntityManaged.Movable)
                .map(e -> (EntityManaged.Movable) e).collect(Collectors.toList());

    }

    public static List<EntityManaged.Movable.Directable> getDirectables(final LevelManaged level) {
        return Levels.getMovables(level).stream().filter(e -> e instanceof EntityManaged.Movable.Directable)
                .map(e -> (EntityManaged.Movable.Directable) e).collect(Collectors.toList());
    }

    public static List<SmartCell> getFreeAreas(final LevelManaged level, final int width, final int height) {
        return level.getCells().stream().filter(c -> Levels.isFreeArea(level, c, width, height))
                .collect(Collectors.toList());
    }

    public static List<SmartCell> getFreeSquares(final LevelManaged level, final int side) {
        return Levels.getFreeAreas(level, side, side);
    }

    public static boolean isFreeArea(final LevelManaged level, final SmartCell cell, final int width,
            final int height) {
        if (!Levels.isFreeCell(level, cell)) {
            return false;
        }
        SmartCell c = cell;
        for (int i = 0; i < width; i++) {
            if (!Levels.isFreeSide(level, c, Direction.DOWN, height)) {
                return false;
            }
            c = level.getSideCell(c, Direction.RIGHT);
        }
        return true;
    }

    public static boolean isFreeSide(final LevelManaged level, final SmartCell cell, final Direction direction,
            final int length) {

        if (direction.getStepCol() != 0 && length >= level.getCols()) {
            return false;
        }
        if (direction.getStepRow() != 0 && length >= level.getRows()) {
            return false;
        }

        if (!Levels.isFreeCell(level, cell)) {
            return false;
        }

        if (Levels.getOccupiedCells(level).contains(cell)) {
            return false;
        }
        if (length == 1) {
            return true;
        }
        return Levels.isFreeSide(level, level.getSideCell(cell, direction), direction, length - 1);
    }

    public static boolean isFreeCell(final LevelManaged level, final SmartCell cell) {
        return level.getEntities().stream().flatMap(e -> e.getCells().stream()).filter(c -> c.equals(cell)).findAny()
                .isEmpty();
    }

}

package it.unibo.biscia.core;

import java.util.List;
import java.util.Map;
import java.util.Set;

enum GameState {
    CONTINUE, RESTART_LEVEL, NEXT_LEVEL, GAMEOVER;
}

interface Judge {

    /**
     * 
     * @param maxFoodCreatedEnergy the max food energi created on level.
     * @param level                the level to judge
     * @param players              list of player on game
     * @param eat                  list of eat occurred
     * @param trimmed              list of entity trimmed
     * @param removed              list of entity removed
     * @param updated              list of entity moved
     */
    void judges(int maxFoodCreatedEnergy, LevelManaged level, List<PlayerManaged> players, Map<Entity, Integer> eat,
            Map<Entity, Integer> trimmed, Set<Entity> removed, Set<Entity> updated);

    /**
     * all entities removed at last judgement.
     * 
     * @return removed entity set
     */
    Set<Entity> getRemoved();

    /**
     * all entities at updated last judgement.
     * 
     * @return updated entity set
     */
    Set<Entity> getUpdated();

    /**
     * all players updated at last judgement.
     * 
     * @return updated playerset
     */
    Set<PlayerManaged> getPlayersUpdates();

    /**
     * the state of game after last judgment, for progress or back or gameover.
     * 
     * @return the state of game
     */
    GameState getState();

    /**
     * true if the level continues and does not contain food.
     * 
     * @return true if level need new food
     */
    boolean addFood();

}

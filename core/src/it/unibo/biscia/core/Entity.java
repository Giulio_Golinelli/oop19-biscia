package it.unibo.biscia.core;

import java.util.List;

/**
 * Is an entity of board, wall or snake.
 *
 */
public interface Entity {

    /**
     * Get the ordered list of cells that compose the entity.
     * 
     * @return a list of cells
     */
    List<Cell> getCells();

    /**
     * define the type of entity, for graphical needs of view and crash reaction.
     * 
     * @return the type of entity
     */
    EntityType getType();

}

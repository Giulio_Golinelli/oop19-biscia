package it.unibo.biscia.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class EntityFactoryImpl implements EntityFactory {

    private final LevelManaged level;

    private abstract class EntityManaged implements it.unibo.biscia.core.EntityManaged {
        private final List<SmartCell> cells;
        private final EntityType type;

        /**
         * the list of cells (required not empty) do not contains duplicate, else throw
         * IllegalArgumentException.
         * 
         * @param cells list of cell that compose entity
         * @param type  entity type to create
         */
        private EntityManaged(final List<SmartCell> cells, final EntityType type) {
            this.cells = validateCells(cells);
            this.type = type;
        }

        private List<SmartCell> validateCells(final List<SmartCell> cells) {
            Objects.requireNonNull(cells);
            if (cells.isEmpty()) {
                throw new IllegalArgumentException("cells needed");
            }
            if (cells.size() > 1 && cells.size() > cells.stream().distinct().count()) {
                throw new IllegalArgumentException("duplicate cells not allowed");
            }
            return cells.stream().collect(Collectors.toList());

        }

        @Override
        public List<Cell> getCells() {

            final List<Cell> ret = new ArrayList<>(this.cells.size());
            for (final var c : this.getSmartCells()) {
                ret.add(c);
            }
            return ret;
        }

        @Override
        public List<SmartCell> getSmartCells() {
            final List<SmartCell> ret;
            synchronized (this.cells) {
                ret = Collections.unmodifiableList(new ArrayList<>(this.cells));
            }
            return ret;
        }

        @Override
        public EntityType getType() {
            return this.type;
        }

        protected boolean addCell(final SmartCell cell) {
            synchronized (this.cells) {
                if (!this.cells.contains(cell)) {
                    this.cells.add(cell);
                    return true;
                }

                return false;
            }
        }

        @Override
        public boolean removeCell(final int index) {
            synchronized (this.cells) {
                if (index >= 0 && index < this.getCells().size()) {

                    this.cells.remove(index);
                    return true;
                }
                return false;
            }

        }

        @Override
        public int removeFromCell(final int index) {
            int ret = 0;
            synchronized (this.cells) {
                while (this.getCells().size() > index) {
                    this.removeCell(this.getCells().size() - 1);
                    ret++;
                }
            }
            return ret;
        }

    }

    /**
     * extends entity for add movements.
     *
     */
    private abstract class Movable extends EntityFactoryImpl.EntityManaged
            implements it.unibo.biscia.core.EntityManaged.Movable {
        private Optional<Direction> direction;
        private final int interval;
        private final MovementType movement;
        private int skipped;

        /**
         * @param cells            for body of entity
         * @param type             type of entity
         * @param direction        initial direction of movement
         * @param movementInterval speed relative to snake
         * @param movement         strategy for movement
         */
        private Movable(final List<SmartCell> cells, final EntityType type, final Optional<Direction> direction,
                final int movementInterval, final MovementType movement) {
            super(cells, type);
            this.direction = direction;
            this.interval = movementInterval;
            this.movement = movement;
            this.skipped = 0;
        }

        @Override
        public boolean move() {
            if (!this.direction.isPresent()) {
                return false;
            }
            skipped++;
            if (skipped < this.interval) {
                return false;
            }
            skipped = 0;
            final var movRes = this.movement.move(this, this.direction.get());
            final List<SmartCell> newCells = movRes.getCells();
            this.direction = movRes.getDirection();
            if (newCells.size() == this.getCells().size()) {
                boolean changed = false;
                for (int i = 0; i < newCells.size() && !changed; i++) {
                    if (!this.getCells().get(i).equals(newCells.get(i))) {
                        changed = true;
                    }
                }
                if (!changed) {
                    return false;
                }
            }
            super.cells.clear();
            newCells.forEach(c -> super.cells.add(c));
            return true;
        }

        @Override
        public final Optional<Direction> getDirection() {
            return this.direction;
        }

    }

    private abstract class Directable extends EntityFactoryImpl.Movable
            implements it.unibo.biscia.core.EntityManaged.Movable.Directable {
        private Directable(final List<SmartCell> cells, final EntityType type, final Optional<Direction> direction,
                final int movementInterval, final MovementType movementType) {
            super(cells, type, direction, movementInterval, movementType);
        }

        @Override
        public void setDirection(final Optional<Direction> direction) {
            // allow direction only if the movement in this direction not crush on second
            // cell of entity.
            if (!super.movement.acceptDirection(this, direction)) {
                return;
            }
            super.direction = direction;
        }
    }

    private abstract class Eatable extends EntityManaged implements it.unibo.biscia.core.EntityManaged.Eatable {
        private final int energy;

        private Eatable(final List<SmartCell> cells, final EntityType type, final int energy) {
            super(cells, type);
            this.energy = energy;
        }

        @Override
        public int getEnergy() {
            return this.energy;
        }
    }

    private final class Snake extends EntityFactoryImpl.Directable implements it.unibo.biscia.core.EntityManaged.Eater {
        private static final int INITIAL_SIZE = 2;
        private int increaseLength;
        private SmartCell lastCellBeforeMovement;

        private Snake(final List<SmartCell> cells, final Optional<Direction> direction, final MovementType movementType,
                final int startEnergy) {
            super(cells, EntityType.SNAKE, direction, 1, movementType);
            this.increaseLength = startEnergy * INCREMENT_FOR_ENERGY;
            if (this.getCells().size() + this.increaseLength < INITIAL_SIZE) {
                this.increaseLength = INITIAL_SIZE - this.getCells().size();
            }
            lastCellBeforeMovement = this.lastCell();
        }

        private SmartCell lastCell() {
            return this.getSmartCells().get(this.getSmartCells().size() - 1);
        }

        @Override
        public EntityType getType() {
            return EntityType.SNAKE;
        }

        @Override
        public boolean move() {
            lastCellBeforeMovement = this.lastCell();
            return super.move();
        }

        @Override
        public void eat(final int energy) {
            this.increaseLength = this.increaseLength + energy * INCREMENT_FOR_ENERGY;
        }

        @Override
        public boolean grow() {
            if (this.increaseLength > 0 && !this.getCells().contains(this.lastCellBeforeMovement)
                    && super.addCell(this.lastCellBeforeMovement)) {
                this.increaseLength--;
                this.lastCellBeforeMovement = this.lastCell();
                return true;
            }
            return false;
        }

    }

    private final class Food extends Eatable {
        private Food(final List<SmartCell> cells, final int energy) {
            super(cells, EntityType.FOOD, energy);
        }
    }

    private final class Wall extends EntityManaged {
        Wall(final List<SmartCell> cells) {
            super(cells, EntityType.WALL);
        }
    }

    private final class WallMobile extends Movable {
        WallMobile(final List<SmartCell> cells, final Optional<Direction> direction, final int movementInterval,
                final MovementType movementType) {
            super(cells, EntityType.WALL, direction, movementInterval, movementType);
        }
    }

    private SmartCell getCasualFreeCell() {
        final List<SmartCell> occ = Levels.getOccupiedCells(level);
        if (occ.size() == this.level.getCols() * this.level.getRows()) {
            throw new IllegalStateException();
        }
        final Random random = new Random();
        SmartCell cell = this.level.getCell(0, 0);
        do {
            cell = this.level.getCell(random.nextInt(level.getCols()), random.nextInt(level.getRows()));
        } while (occ.contains(cell));
        return cell;
    }

    EntityFactoryImpl(final LevelManaged level) {
        this.level = level;

    }

    @Override
    public it.unibo.biscia.core.EntityManaged.Movable.Directable makeBabySnake(final boolean runEver) {
        Direction direction;
        switch (Levels.getDirectables(level).size()) {
        case 0:
            direction = Direction.RIGHT;
            break;
        case 1:
            direction = Direction.LEFT;
            break;
        case 2:
            direction = Direction.UP;
            break;
        case 3:
            direction = Direction.DOWN;
            break;
        default:
            direction = Direction.values()[new Random().nextInt(Direction.values().length)];
        }
        return makeAdultSnake(Arrays.asList(Levels.getNewSnakePosition(level)), 0,
                runEver ? Optional.of(direction) : Optional.empty(), runEver);
    }

    @Override
    public it.unibo.biscia.core.EntityManaged.Movable.Directable makeAdultSnake(final List<SmartCell> cells,
            final int energy, final Optional<Direction> direction, final boolean runEver) {
        return new Snake(cells, direction, runEver ? MovementType.SLITHER : MovementType.SLITHER_ONE_STEP, 0);
    }

    @Override
    public it.unibo.biscia.core.EntityManaged.Eatable makeCasualFood(final int energy) {

        final List<SmartCell> cells = Arrays.asList(this.getCasualFreeCell());
        return new Food(cells, energy);
    }

    @Override
    public it.unibo.biscia.core.EntityManaged makeEdge() {
        return makeWall(
                this.level
                        .getCells().stream().filter(c -> c.getCol() == 0 || c.getCol() == this.level.getCols() - 1
                                || c.getRow() == 0 || c.getRow() == this.level.getRows() - 1)
                        .collect(Collectors.toList()));
    }

    @Override
    public it.unibo.biscia.core.EntityManaged makeLinearWall(final SmartCell start, final int length,
            final Direction direction) {
        return makeWall(Stream.iterate(start, cell -> this.level.getSideCell(cell, direction)).limit(length)
                .collect(Collectors.toList()));
    }

    @Override
    public it.unibo.biscia.core.EntityManaged makeWall(final List<SmartCell> cells) {
        return new Wall(cells);
    }

    @Override
    public it.unibo.biscia.core.EntityManaged.Movable makeMovableWall(final List<SmartCell> cells,
            final Direction direction, final MovementType movementType, final int movementInterval) {
        return new WallMobile(cells, Optional.of(direction), movementInterval, movementType);
    }

}

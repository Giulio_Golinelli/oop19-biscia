package it.unibo.biscia.core;

import java.util.Map;
import java.util.Set;

interface InteractionManager {

    /**
     * apply the movement and commands on level passed.
     * 
     * @param level    level to move
     * @param commands command to apply
     * @return true if any change is performed
     */
    boolean performMovement(LevelManaged level, Map<EntityManaged.Movable.Directable, Direction> commands);

    /**
     * list of eat on last movement.
     * 
     * @return Map of entity to eat and energy assumed
     */
    Map<Entity, Integer> getEat();

    /**
     * list of entity trimmed on last movement..
     * 
     * @return entity that eat eatable
     */
    Map<Entity, Integer> getTrimmed();

    /**
     * list of removed entity on last movement (eat or crush).
     * 
     * @return entities removed from level
     */
    Set<Entity> getRemoved();

    /**
     * list of entity moved.
     * 
     * @return entities update after movement
     */
    Set<Entity> getUpdated();

}

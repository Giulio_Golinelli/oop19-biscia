package it.unibo.biscia.core;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class InteractionManagerlImpl implements InteractionManager {
    private LevelManaged level;
    // private final LevelAnalyzer analyzer;
    private final Map<Entity, Integer> eat = new HashMap<>();
    private final Map<Entity, Integer> trimmed = new HashMap<>();
    private final Set<Entity> removed = new HashSet<>();
    private final Set<Entity> updated = new HashSet<>();

    private Set<EntityManaged> getIntersect(final Entity entity, final Set<EntityManaged> inter) {
        return inter.stream().filter(e -> !e.equals(entity))
                .filter(e -> e.getCells().stream().filter(c -> entity.getCells().contains(c)).findAny().isPresent())
                .collect(Collectors.toSet());
    }

    private Set<EntityManaged> getIntersect(final Entity entity) {
        return getIntersect(entity, this.getEntityIntrsecated());
    }

    private Set<EntityManaged> getEntityIntrsecated() {
        return Levels.getIntersecatedEntity(this.level).stream().collect(Collectors.toSet());
    }

    private Stream<EntityManaged> filterForInterfaces(final Stream<EntityManaged> stream,
            final Class<? extends EntityManaged> filters) {
        return stream.filter(e -> filters.isInstance(e));
    }

    private Stream<EntityManaged> filterForInterfaces(final Stream<EntityManaged> entities,
            final List<Class<? extends EntityManaged>> types) {
        var ret = entities;
        for (final var t : types) {
            ret = this.filterForInterfaces(ret, t);
        }
        return ret;
    }

    private Stream<EntityManaged> filterForType(final Stream<EntityManaged> entities, final EntityType type) {
        return entities.filter(e -> e.getType().equals(type));
    }

    private boolean detectEat(final Set<EntityManaged> inter) {
        final int removed = this.removed.size();
        // determino le intersezioni col cibo di:
        // prima gli eater directable (ai mangiatori pilotati do' la precedenza)
        // poi gli eater movable (poi i mangiatori che si muovono)
        // poi gli eater (tutti i mangiatori residui)
        // e alla fine tutti gli elementi, che non mangiano ma schiacciano (e quindi
        // distruggono( il cibo
        final List<Class<? extends EntityManaged>> filters = new LinkedList<>(Arrays.asList(EntityManaged.Eater.class,
                EntityManaged.Movable.class, EntityManaged.Movable.Directable.class));
        boolean exit = false;
        while (!exit) {
            this.filterForInterfaces(inter.stream(), filters).forEach(eater -> {
                this.getIntersect(eater).stream().forEach(eatable -> {
                    if (eatable.getType().equals(EntityType.FOOD) || eatable instanceof EntityManaged.Eatable) {
                        this.removed.add(eatable);
                        this.level.removeEntity(eatable);
                        if (eater instanceof EntityManaged.Eater) {
                            final int energy = ((EntityManaged.Eatable) eatable).getEnergy();
                            ((EntityManaged.Eater) eater).eat(energy);
                            this.updated.add(eater);
                            this.eat.put(eater, energy + this.eat.getOrDefault(eater, 0));
                        }
                        this.updated.remove(eatable);
                    }
                });
            });
            if (filters.isEmpty()) {
                exit = true;
            } else {
                filters.remove(filters.size() - 1);
            }
        }

        return this.removed.size() > removed;
        /*
         * for (int i = 0; i < 4; i++) { final int j = i; inter.stream().filter(e -> (j
         * > 1) || e instanceof Entity.EntityManaged.Eater) .filter(e -> (j > 0) || e
         * instanceof Entity.EntityManaged.Movable.Directable).forEach(eater -> {
         * this.getIntersect(eater).stream() .filter(eatable -> eatable instanceof
         * Entity.EntityManaged.Eatable).forEach(eatable -> { this.removed.add(eatable);
         * this.coreLevel.removeEntity(eatable); if (eater instanceof
         * Entity.EntityManaged.Eater) { int energy = ((Entity.EntityManaged.Eatable)
         * eatable).getEnergy(); ((Entity.EntityManaged.Eater) eater).eat(energy);
         * this.updated.add(eater); this.eat.put(eater, energy +
         * this.eat.getOrDefault(eater, 0)); } this.updated.remove(eatable); }); }); }
         */
        // return (this.removed.size() < removed);

    }

    private boolean detectDead(final Set<EntityManaged> inter) {
        // in questo punto food ed eatable in genere son spariti, tutti gli scontri di
        // snake con la testa
        // portano alla morte singolarmente, quindi le eliminazioni le faccio alla fine
        this.filterForType(inter.stream(), EntityType.SNAKE).forEach(snake -> {
            // determino lo scontro col proprio corpo
            boolean crash = false;
            final SmartCell head = snake.getSmartCells().get(0);
            for (int i = 1; i < snake.getSmartCells().size() && !crash; i++) {
                if (snake.getSmartCells().get(i).equals(head)) {
                    crash = true;
                }
            }
            if (!crash) {
                // determino lo scontro con altre entità
                crash = Levels.entitiesOnCell(this.level, snake.getSmartCells().get(0)).size() > 1;
            }
            if (crash) {
                removed.add(snake);
                this.level.removeEntity(snake);
                this.updated.remove(snake);
            }
        });
        this.removed.addAll(removed);
        return !removed.isEmpty();

    }

    private void addTrim(final EntityManaged entity, final int trimmed) {
        if (entity.getSmartCells().isEmpty()) {
            this.removed.add(entity);
            this.updated.remove(entity);
        } else {
            this.trimmed.put(entity, trimmed + this.trimmed.getOrDefault(entity, 0));
            this.updated.add(entity);
        }

    }

    private boolean detectTrim(final Set<EntityManaged> inter) {
        // tutti i wall che intersecano degli snake (a questo punto non nella testa),
        // tagliamno via la parte di snake verso la coda
        boolean trimmed = false;
        final var it = this.filterForType(inter.stream(), EntityType.SNAKE).collect(Collectors.toList());
        for (final var snake : it) {
            for (int i = 1; i < snake.getCells().size(); i++) {
                final SmartCell cell = snake.getSmartCells().get(i);
                if (this.filterForType(inter.stream(), EntityType.WALL).flatMap(wall -> wall.getSmartCells().stream())
                        .filter(c -> c.equals(cell)).findAny().isPresent()) {
                    addTrim(snake, snake.removeFromCell(i));
                    trimmed = true;
                }
            }
        }
        return trimmed;
    }

    private boolean detectDestroy(final Set<EntityManaged> inter) {
        // tutti i wall che intersecano altre cose (a questo punto altri wall) perdono
        // le celle che intersecano
        // verifico le celle soggette prima poi le elimino, così le elimino da antrambi
        // i wall
        final Set<SmartCell> cells = Levels.getIntersectionsCells(this.level).stream().collect(Collectors.toSet());
        if (cells.isEmpty()) {
            return false;
        }
        cells.forEach(cell -> {
            inter.stream().filter(e -> e.getCells().contains(cell)).forEach(e -> {
                e.removeCell(e.getSmartCells().indexOf(cell));
                if (e.getSmartCells().isEmpty()) {
                    this.removed.add(e);
                    this.updated.remove(e);
                    this.eat.remove(e);
                } else {
                    this.trimmed.put(e, 1 + this.trimmed.getOrDefault(e, 0));
                    this.updated.add(e);
                }
            });
        });
        return true;
    }

    @Override
    public Map<Entity, Integer> getEat() {
        return this.eat;
    }

    @Override
    public Map<Entity, Integer> getTrimmed() {
        return this.trimmed;
    }

    @Override
    public Set<Entity> getRemoved() {
        return this.removed;
    }

    @Override
    public Set<Entity> getUpdated() {
        return this.updated;
    }

    private Set<EntityManaged.Movable> moveAllMoveable(
            final Map<EntityManaged.Movable.Directable, Direction> commands) {
        // set direction to directable elements
        commands.forEach((entity, direction) -> {
            if (entity instanceof EntityManaged.Movable.Directable) {
                ((EntityManaged.Movable.Directable) entity).setDirection(Optional.of(direction));
            }
        });
        return Levels.getMovables(this.level).stream().filter(m -> m.getDirection().isPresent()).filter(m -> m.move())
                .collect(Collectors.toSet());
    }

    private Set<EntityManaged.Eater> growAllEater() {
        return Levels.getEater(this.level).stream().filter(e -> e.grow()).collect(Collectors.toSet());
    }

    @Override
    public boolean performMovement(final LevelManaged level,
            final Map<EntityManaged.Movable.Directable, Direction> commands) {
        this.level = level;
        this.eat.clear();
        this.removed.clear();
        this.updated.clear();
        this.trimmed.clear();
        // muovo i moveable
        this.moveAllMoveable(commands).forEach(e -> this.updated.add(e));
        // faccio crescere gli eater
        this.growAllEater().forEach(e -> this.updated.add(e));
        if (this.updated.isEmpty()) {
            return false;
        }
        Set<EntityManaged> inter = this.getEntityIntrsecated();
        // se non ci sono intersezioni esco con true
        if (inter.isEmpty()) {
            return true;
        }
        // gestisco le mangiate
        if (this.detectEat(inter)) {
            inter = this.getEntityIntrsecated();
            if (inter.isEmpty()) {
                return true;
            }
        }
        // gestisco la morte degli snake
        if (this.detectDead(inter)) {
            inter = this.getEntityIntrsecated();
            if (inter.isEmpty()) {
                return true;
            }
        }
        // gestisco il trim degli snake
        if (this.detectTrim(inter)) {
            inter = this.getEntityIntrsecated();
            if (inter.isEmpty()) {
                return true;
            }
        }

        if (this.detectDestroy(inter)) {
            inter = this.getEntityIntrsecated();
            if (inter.isEmpty()) {
                return true;
            }
        }
        return true;
    }

}

package it.unibo.biscia.core;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

final class SmartCellImpl implements SmartCell {
    private final int col;
    private final int row;
    private String toStringCache;
    private final int hashCodeCache;
    private LevelManaged level;

    /**
     * basic constructor.
     * 
     * @param col positive column index of cell
     * @param row positive row index of cell
     */
    SmartCellImpl(final int col, final int row) {
        if (col < 0 || row < 0) {
            throw new IllegalArgumentException();
        }
        this.col = col;
        this.row = row;
        hashCodeCache = this.calcHashCode();
    }

    /**
     * constructor with level for better smart implementation.
     * 
     * @param level level over is this cell
     * @param col   positive column index of cell
     * @param row   positive row index of cell
     */
    SmartCellImpl(final LevelManaged level, final int col, final int row) {
        this(col, row);
        this.level = level;
    }

    @Override
    public int getCol() {
        return this.col;
    }

    @Override
    public int getRow() {
        return this.row;
    }

    private int calcHashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + col;
        result = prime * result + row;
        return result;
    }

    @Override
    public int hashCode() {
        return this.hashCodeCache;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SmartCellImpl other = (SmartCellImpl) obj;
        if (col != other.col) {
            return false;
        }
        return row == other.row;
    }

    @Override
    public String toString() {
        // calculate only if need
        if (Objects.isNull(this.toStringCache)) {
            this.toStringCache = "[" + col + ", " + row + "]";
        }
        return this.toStringCache;
    }

    @Override
    public SmartCell geSideCell(final Direction direction) {
        if (Objects.isNull(this.level)) {
            int col = this.getCol() + direction.getStepCol();
            int row = this.getRow() + direction.getStepRow();
            if (col < 0) {
                col = LevelManaged.MIN_COLS;
            }
            if (row < 0) {
                row = LevelManaged.MIN_ROWS;
            }
            return new SmartCellImpl(col, row);
        }
        return this.level.getSideCell(this, direction);
    }

    @Override
    public Set<EntityManaged> getContainedEntities() {
        if (Objects.isNull(this.level)) {
            return Collections.emptySet();
        }
        return Collections.unmodifiableSet(this.level.getEntitiesManaged().stream()
                .filter(e -> e.getCells().contains(this)).collect(Collectors.toSet()));
    }
}

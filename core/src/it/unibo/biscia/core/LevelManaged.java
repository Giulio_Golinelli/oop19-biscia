package it.unibo.biscia.core;

import java.util.List;

/**
 * Additional functions for internal use.
 *
 */
interface LevelManaged extends Level {

    void setCardinal(int cardinal);

    List<EntityManaged> getEntitiesManaged();

    /**
     * Get a single cell on board.
     * 
     * @param col index of column
     * @param row index of row
     * @return a single cell at coordinate indicated
     */
    SmartCell getCell(int col, int row);

    /**
     * return the cell to the side of passed cell.
     * 
     * @param col       column index of cell to start
     * @param row       row index of cell to start
     * @param direction side to request
     * @return cell at side indicated
     */
    SmartCell getSideCell(int col, int row, Direction direction);

    /**
     * return the cell to the side of passed cell.
     * 
     * @param cell      cell to start
     * @param direction side to request
     * @return cell at side indicated
     */
    SmartCell getSideCell(SmartCell cell, Direction direction);

    /**
     * return the full sequences of cells.
     * 
     * @return list of all cells grid
     */
    List<SmartCell> getCells();

    /**
     * for require a delimited subset of cells of level.
     * 
     * @param cell   cell of start or area
     * @param width  width of area
     * @param height height of area
     * @return a list with all cells of area definited
     */
    List<SmartCell> getArea(SmartCell cell, int width, int height);

    /**
     * for require a delimited subset of cells of level.
     * 
     * @param cell1 cell of first corner of area
     * @param cell2 cell of second corner of area
     * @return a list with all cells of area definited
     */
    List<SmartCell> getArea(SmartCell cell1, SmartCell cell2);

    /**
     * add entity to a level. If the cells of new entity is occpupies then throw
     * IllegalArgumentException.
     * 
     * @param entity entity to add
     */
    void addEntity(EntityManaged entity);

    /**
     * remove entity to level.
     * 
     * @param entity entity to remove
     */
    void removeEntity(EntityManaged entity);
}

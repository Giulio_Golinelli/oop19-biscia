package it.unibo.biscia.core;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

final class DirectionHarvesterImpl implements DirectionHarvester {
    private final Map<EntityManaged.Movable.Directable, Direction> commands = new LinkedHashMap<>();

    @Override
    public Map<EntityManaged.Movable.Directable, Direction> getCommands() {
        synchronized (this.commands) {
            final var ret = Collections.unmodifiableMap(this.commands.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
            this.commands.clear();
            return ret;
        }

    }

    @Override
    public void setDirection(final EntityManaged.Movable.Directable directable, final Direction direction) {
        Objects.requireNonNull(directable);
        Objects.requireNonNull(direction);
        synchronized (this.commands) {
            this.commands.put(directable, direction);
        }
    }

}

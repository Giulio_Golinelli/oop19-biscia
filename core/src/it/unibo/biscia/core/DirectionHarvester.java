package it.unibo.biscia.core;

import java.util.Map;

/**
 * define interface for harvest commads to return them as needed.
 *
 */
interface DirectionHarvester {

    /**
     * return all commands harvested from last getCommands, then reset command.
     * collection
     * 
     * @return all command
     */
    Map<EntityManaged.Movable.Directable, Direction> getCommands();

    /**
     * called for adding a player direction to command collection.
     * 
     * @param directable entity to command
     * @param direction  direction of command
     */
    void setDirection(EntityManaged.Movable.Directable directable, Direction direction);

}

package it.unibo.biscia.events;

/**
 * for register and remove observer of general modify to a state of game.
 *
 */
public interface StateSubject extends GenericObserverCollector<StateObserver> {

}

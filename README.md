# Biscia

Biscia is a Java clone of the historical game [Nibbles](https://en.wikipedia.org/wiki/Nibbles_(video_game)) for MS-DOS.

## Importing the project
To import the source code follow these instructions:
* Clone this repository
* Import the root folder of the repo via Gradle into your IDE

## Running
**Important**: To run successfully in any way the game you must have a Java version >= 11

To launch the game, you can either import the project and run *DesktopLauncher* class inside the desktop sub-project with your IDE or you can clone the repo and execute ```./gradlew desktop:run``` Gradle task inside the root project folder.

**NOTE**: You can also download and run the jar file attached to this repo.

## Testing
If you have imported the project you can test every test class inside the tests sub-project folder via JUnit4.
If you only have cloned the repo you can run ```./gradlew tests:test``` Gradle task inside the root project folder.

**NOTE**: Gradle doesn't rerun successful tests so if you want to execute all of them, again and again, you have to type ```./gradlew --rerun-tasks tests:test```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)